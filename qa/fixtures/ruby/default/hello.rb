def some_rails_controller
  foo = params[:some_regex]
  Regexp.new(foo).match("some_string")
end

def use_params_in_regex
  @x = something.match /#{params[:x]}/
end

def regex_on_params
  @x = params[:x].match /foo/
end

some_rails_controller