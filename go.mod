module gitlab.com/gitlab-org/security-products/analyzers/semgrep/v4

go 1.15

require (
	github.com/pelletier/go-toml v1.9.5 // indirect
	github.com/sirupsen/logrus v1.9.3
	github.com/stretchr/testify v1.9.0
	github.com/urfave/cli/v2 v2.27.1
	gitlab.com/gitlab-org/security-products/analyzers/command/v2 v2.2.0
	gitlab.com/gitlab-org/security-products/analyzers/common/v3 v3.2.3
	gitlab.com/gitlab-org/security-products/analyzers/report/v4 v4.3.2
	gitlab.com/gitlab-org/security-products/analyzers/ruleset v1.4.1 // indirect
	gitlab.com/gitlab-org/security-products/analyzers/ruleset/v2 v2.0.9
	gopkg.in/yaml.v2 v2.4.0
)
